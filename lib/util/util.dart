import 'dart:convert';

import 'package:flutter/services.dart';

class Util {
  static Future<String> loadCapitulosFromAsset() async {
    return await rootBundle.loadString("assets/arquivos/capitulos.json");
  }

  static Future getCapitulosJson() async {
    String jsonString = await loadCapitulosFromAsset();
    var jsonResponse = jsonDecode(jsonString);

    List<String> lista = List.from(jsonResponse);

    return lista;
  }
}