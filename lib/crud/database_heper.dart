import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:guarde_a_palavra/models/dataModel.dart';

class DataBaseHelper {

  FirebaseUser user;

  saveData(DataModel data, BuildContext context) async {
    user = await FirebaseAuth.instance.currentUser();
    data.id = Firestore.instance
      .collection("users")
      .document(user.uid)
      .collection('events_data').document().documentID;

    Firestore.instance
      .collection("users")
      .document(user.uid)
      .collection('events_data')
      .document(data.id)
      .setData({
        "id": data.id,
        "tipoEvento": data.tipoEvento,
        "nomeCapitulo": data.nomeCapitulo,
        "numeroCapituloInicial": data.numeroCapituloInicial,
        "numeroVersiculoInicial": data.numeroVersiculoInicial,
        "numeroVersiculoFinal": data.numeroVersiculoFinal,
        "cidade": data.cidade,
        "localCidadeOuIgreja": data.localCidadeOuIgreja,
        "data": data.data,
        "comentario": data.comentario
      })
      .then((result) => {
        Navigator.pop(context),
      })
      .catchError((err) => print(err));
  }
}