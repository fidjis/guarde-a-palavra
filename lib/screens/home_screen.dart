import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:guarde_a_palavra/my_widgets/data_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_admob/firebase_admob.dart';

class HomeScreen extends StatefulWidget {

  HomeScreen({this.uid});

  final String uid;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  var primaryColor = const Color(0xFFda5e5a);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFe89b98);

  void initState() {
    super.initState();
    FirebaseAdMob.instance.initialize(appId: "ca-app-pub-9530069764203602~1992377793");
  }

  @override
  void dispose() {
    myBanner?.dispose();
    myInterstitial?.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    myBanner
      // typically this happens well before the ad is shown
      ..load()
      ..show(
        // Positions the banner ad 60 pixels from the bottom of the screen
        anchorOffset: 20.0,
        // Positions the banner ad 10 pixels from the center of the screen to the right
        //horizontalCenterOffset: 10.0,
        // Banner Position
        anchorType: AnchorType.top,
      );
    myInterstitial
      ..load()
      ..show(
        anchorType: AnchorType.bottom,
        anchorOffset: 0.0,
        horizontalCenterOffset: 0.0,
      );

    return MaterialApp(
      home: Scaffold(
        backgroundColor: terciaryColor,
        appBar: AppBar(
          backgroundColor: terciaryColor,
          title: Divider(color: primaryColor,),
        ),
        body: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            buildBackgroundAppName(),
            buildBackgroundAppNameShadow(),
            //Positioned(
            //  top: 70.0,
            //  right: 70.0,
            //  child: new Image(
            //    image: new AssetImage("assets/images/app_logo.png"),
            //    height: 92.0,
            //    width: 92.0,
            //  ),
            //),
            buildDraggableScrollableSheet(),
          ],
        ),
      ),
    );
  }

  Padding buildBackgroundAppName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 15.0, ),
      child: Text(
        "Guarde a\n    Palavra",
        style: TextStyle(
          color: primaryColor,
          fontSize: 50,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Padding buildBackgroundAppNameShadow() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 35.0, 20.0, 20.0,),
      child: Text(
        "Guarde a\n    Palavra",
        style: TextStyle(
          color: Colors.white,
          fontSize: 50,
          fontFamily: "Lobster",
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Container buildDraggableScrollableSheet() {
    return Container(
      child: DraggableScrollableSheet(
        initialChildSize: 0.7,
        minChildSize: 0.7,
        maxChildSize: 1.0,
        builder: (BuildContext context, myscrollController) {
          return Column(
            children: <Widget>[
              buildDragCabecalho(),
              buildAnuncioBody(),
              Expanded(child: Container(
                decoration: new BoxDecoration(
                  color: segundaryColor,
                  borderRadius: new BorderRadius.only(
                    topLeft:  const  Radius.circular(40.0),
                    topRight: const  Radius.circular(40.0))
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: StreamBuilder(
                    stream: Firestore.instance.collection('users')
                      .document(widget.uid)
                      .collection('events_data')
                      .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return CircularProgressIndicator();
                      }
                      return ListView.builder(
                        controller: myscrollController,
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, index) {
                          DocumentSnapshot doc = snapshot.data.documents[index];
                          return DataWidget(
                            tipoEvento: doc['tipoEvento'], 
                            livro: doc['nomeCapitulo'],
                            data: doc['data'], 
                            cidade: doc['cidade'], 
                            bairro: doc['localCidadeOuIgreja'],  
                            numCapitulo: doc['numeroCapituloInicial'].toString(), 
                            numVersiculoInicial: doc['numeroVersiculoInicial'].toString(),  
                            numVersiculoFinal: doc['numeroVersiculoFinal'].toString(), 
                          );
                        },
                      );
                    }
                  ),
                ),
              )),
            ],
          );
        },
      ),
    );
  }

  Container buildDragCabecalho() {
    return Container(
      color: terciaryColor,
      child: Stack(
        children: <Widget>[
          buildBiblicMessage(),
          Positioned(
            right: 10.0,
            bottom: 10.0,
            child: FloatingActionButton.extended(
              onPressed: ()=> Navigator.pushNamed(context, '/new_data'),
              //onPressed: ()=> Navigator.pushNamed(context, '/view_data'),
              heroTag: "login_google",
              backgroundColor: primaryColor,
              icon: Icon(Icons.add_circle_outline),
              label: Text("Nova Palavra")
            ),
          ),
        ],
      ),
    );
  }

  Container buildBiblicMessage() {
    return Container(
      padding: EdgeInsets.only(bottom: 20.0),
      height: 120.0,
      margin: const EdgeInsets.fromLTRB(0, 24, 25, 20),
      child: new Stack(
        children: <Widget>[
          Container(
            height: 124.0,
            margin: new EdgeInsets.only(left: 46.0),
            padding: const EdgeInsets.fromLTRB(50, 25, 25, 20),
            decoration: new BoxDecoration(
              color: Colors.orange[700],
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(8.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(
                  color: Colors.black12,
                  blurRadius: 10.0,
                  offset: new Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Center(
              child: Wrap(
                children: <Widget>[
                  SelectableText(
                    "Deus seja louvado!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: segundaryColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(
              vertical: 16.0
            ),
            alignment: FractionalOffset.centerLeft,
            child: new Image(
            //child: new Image.network(
              //"https://raw.githubusercontent.com/sergiandreplace/planets-flutter/master/assets/img/mars.png",
              image: new AssetImage("assets/images/app_logo.png"),
              height: 92.0,
              width: 92.0,
            ),
          ),
        ],
      )
    );
  }

  buildAnuncioBody() {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: AdmobBanner(
        adUnitId: "ca-app-pub-3940256099942544/6300978111",
        adSize: AdmobBannerSize.BANNER,
        //listener:
        //    (AdmobAdEvent event, Map<String, dynamic> args) {
        //  handleEvent(event, args, 'Banner');
        //},
      ),
    );
  }
}

MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
  keywords: <String>['church', 'Bible'],
  contentUrl: 'https://flutter.io',
  childDirected: false,// or MobileAdGender.female, MobileAdGender.unknown
  testDevices: <String>[], // Android emulators are considered test devices
);

BannerAd myBanner = BannerAd(
  // Replace the testAdUnitId with an ad unit id from the AdMob dash.
  // https://developers.google.com/admob/android/test-ads
  // https://developers.google.com/admob/ios/test-ads
  adUnitId: BannerAd.testAdUnitId,
  size: AdSize.smartBanner,
  targetingInfo: targetingInfo,
  listener: (MobileAdEvent event) {
    print("BannerAd event is $event");
  },
);

InterstitialAd myInterstitial = InterstitialAd(
  // Replace the testAdUnitId with an ad unit id from the AdMob dash.
  // https://developers.google.com/admob/android/test-ads
  // https://developers.google.com/admob/ios/test-ads
  adUnitId: InterstitialAd.testAdUnitId,
  targetingInfo: targetingInfo,
  listener: (MobileAdEvent event) {
    print("InterstitialAd event is $event");
  },
);