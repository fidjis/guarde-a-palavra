import 'package:flutter/material.dart';
import 'package:guarde_a_palavra/models/dataModel.dart';
import 'package:guarde_a_palavra/models/item_expanded_model.dart';

class ViewDataScreen extends StatefulWidget {

  ViewDataScreen(this.data);

  final DataModel data;

  @override
  _ViewDataScreenState createState() => _ViewDataScreenState();
}

class _ViewDataScreenState extends State<ViewDataScreen> {

  var primaryColor = const Color(0xFFda5e5a);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFe89b98);

  List<MyItemExpanded> _items;

  @override
  void initState() {
    super.initState();
    _items = List<MyItemExpanded>();
    _items.add(
      MyItemExpanded(
        isExpanded: false,
        TAG: "tag",
        header: MyHeader(headerName: "${widget.data.nomeCapitulo}"),
        body: MyItemBody(name: "Cap ${widget.data.numeroCapituloInicial}. v${widget.data.numeroVersiculoInicial} ao v${widget.data.numeroVersiculoFinal}")
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            centerTitle: true,
            backgroundColor: terciaryColor,
            pinned: true,
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              title: buildTitleText(),
              centerTitle: true,
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  buildPage("${widget.data.tipoEvento}"),
                  SizedBox(height: 50,),
                ],
              )
            ]),
          )
        ],
      ),
    );
  }

  Column buildPage(String event) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Card(
          margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 2.0),
          color: primaryColor,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(15.0),
                  child: Text(
                    event,
                    style: TextStyle(
                      fontSize: 20,
                      color: segundaryColor
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),

        Card(
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                color: terciaryColor,
                child: Text(
                  "Comentario:",
                  style: TextStyle(
                    fontSize: 16,
                    color: segundaryColor,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
                  title: Text(
                    "${widget.data.comentario}",
                    style: TextStyle(color: primaryColor, fontSize: 14),
                  ),
                ),
              ),
              
            ],
          ),
        ),
        Card(
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                color: terciaryColor,
                //color: Colors.grey[400],
                child: Text(
                  "Cidade:",
                  style: TextStyle(
                    fontSize: 16,
                    color: segundaryColor,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
                  title: Text(
                    "${widget.data.cidade}",
                    style: TextStyle(color: primaryColor, fontSize: 14),
                  ),
                ),
              ),
              
            ],
          ),
        ),
        Card(
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                color: terciaryColor,
                //color: Colors.grey[400],
                child: Text(
                  "Local:",
                  style: TextStyle(
                    fontSize: 16,
                    color: segundaryColor,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
                  title: Text(
                    "${widget.data.localCidadeOuIgreja}",
                    style: TextStyle(color: primaryColor, fontSize: 14),
                  ),
                ),
              ),
              
            ],
          ),
        ),
        Card(
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                color: terciaryColor,
                //color: Colors.grey[400],
                child: Text(
                  "Data:",
                  style: TextStyle(
                    fontSize: 16,
                    color: segundaryColor,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
                  title: Text(
                    "${widget.data.data}",
                    style: TextStyle(color: primaryColor, fontSize: 14),
                  ),
                ),
              ),
            ],
          ),
        ),
        MyHeader(headerName: "${widget.data.nomeCapitulo}"),
        MyItemBody(name: "Cap ${widget.data.numeroCapituloInicial}. v${widget.data.numeroVersiculoInicial} ao v${widget.data.numeroVersiculoFinal}"),
        //buildExpansionPanelList(),
      ],
    );
  }

  ExpansionPanelList buildExpansionPanelList() {
    return ExpansionPanelList(
        expansionCallback: (int index, bool isExpanded) {
          setState(() {
            _items[index].isExpanded = !_items[index].isExpanded;
          });
        },
        children: _items.map((MyItemExpanded item) {
          return ExpansionPanel(
            headerBuilder: (BuildContext context, bool isExpanded) {
              return item.header;
            },
            isExpanded: item.isExpanded,
            body: item.body,
          );
        }).toList(),
      );
  }

  buildTitleText() {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0,),
          child: Text(
            "Guarde a Palavra",
            style: TextStyle(
              color: primaryColor,
              fontSize: 30,
              fontFamily: "Lobster",
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0,),
          child: Text(
            "Guarde a Palavra",
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontFamily: "Lobster",
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }

  Stack buildStack(BuildContext context) {
    return Stack(alignment: Alignment.topCenter, children: <Widget>[
      Container(
        height: MediaQuery.of(context).size.height * 0.3,
        child: Material(
          //color: terciaryColor,
          borderRadius: BorderRadius.circular(10.0),
          elevation: 2.0,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Container(color: terciaryColor,),
        ),
      ),
      ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ListTile(title: Text(index.toString()),);
        },
        itemCount: 30,
      ),
    ],);
  }
}