import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:guarde_a_palavra/crud/database_heper.dart';
import 'package:guarde_a_palavra/models/dataModel.dart';

  var primaryColor = const Color(0xFFda5e5a);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFe89b98);

  //TODO: colocar isso em uma classe ou ENUM, etc
  bool validateBuildTipoEventoWidget = false;
  bool validateCapitulosWidget = false;
  bool validateLocalWidget1 = false;
  bool validateLocalWidget2 = false;
  bool validateLocalWidget3 = false;
  bool validateComentarioWidget = false;
  bool validateSaveProgressWidget = true;
  int atualWidgetID = 1;

class NewData extends StatefulWidget {
  @override
  _NewDataState createState() => _NewDataState();
}

class _NewDataState extends State<NewData> {
  
  PageController controller = PageController();
  EasingAnimationWidget animation1 = EasingAnimationWidget();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: terciaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              animation1,
              buildButtons(),
            ],
          ),
        ),
      ),
    );
  }

  buildButtons() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(child: FloatingActionButton(
              backgroundColor: segundaryColor,
              onPressed: (){
                animation1.state.moverOutForm(forBack: true);  
              },
              mini: true,
              child: Icon(Icons.arrow_left,color: primaryColor,),
              heroTag: "ffff",
            ),),
            Expanded(child: FloatingActionButton.extended(
              backgroundColor: segundaryColor,
              onPressed: (){
                print(atualWidgetID);
                print(validateBuildTipoEventoWidget);
                switch (atualWidgetID) {
                  case 1:
                    if(validateBuildTipoEventoWidget)
                      animation1.state.moverOutForm(forBack: false);
                    else
                      showInSnackBar();
                    break;
                  case 2:
                    if(validateCapitulosWidget)
                      animation1.state.moverOutForm(forBack: false);  
                    else
                      showInSnackBar();
                    break;
                  case 3:
                    if(validateLocalWidget1 && validateLocalWidget2 && validateLocalWidget3)
                      animation1.state.moverOutForm(forBack: false);  
                    else
                      showInSnackBar();
                    break;
                  case 4:
                    if(validateComentarioWidget)
                      animation1.state.moverOutForm(forBack: false);  
                    else
                      showInSnackBar();
                    break;
                  case 5:
                    if(validateSaveProgressWidget)
                      animation1.state.moverOutForm(forBack: false);  
                    else
                      showInSnackBar();
                    break;
                  default:
                    break;
                }

                //animation1.state.moverOutForm(forBack: false);  
              },
              heroTag: "login_google",
              label: Text("Salvar", style: TextStyle(color: primaryColor),)
            ),),
            Expanded(child: Container(),)
          ],
        ),
        SizedBox(height: 30,),
        FlatButton(
          onPressed: (){
            validateBuildTipoEventoWidget = false;
            validateCapitulosWidget = false;
            validateLocalWidget1 = false;
            validateLocalWidget2 = false;
            validateLocalWidget3 = false;
            validateComentarioWidget = false;
            validateSaveProgressWidget = true;
            atualWidgetID = 1;

            Navigator.pop(context);
          },
          child: Text(
            "Cancelar",
            style: TextStyle(
              decoration: TextDecoration.underline,
              color: segundaryColor
            ),
          )
        ),
      ],
    );
  }

  void showInSnackBar() {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        "Preencha os Campos",
        textAlign: TextAlign.center,
        style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontFamily: "WorkSansSemiBold"),
      ),
      backgroundColor: terciaryColor,
      duration: Duration(seconds: 3),
    ));
  }
}

class EasingAnimationWidget extends StatefulWidget {

  EasingAnimationWidgetState state = EasingAnimationWidgetState();

  @override
  EasingAnimationWidgetState createState() => state;
}

class EasingAnimationWidgetState extends State<EasingAnimationWidget>
    with TickerProviderStateMixin {

  FormNewDataWidget form = FormNewDataWidget();

  AnimationController _controller;
  Animation _animation;

  AnimationController _controller2;
  Animation _animation2;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _animation = Tween(begin: -1.0, end: 0.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ))..addStatusListener(handler);
    //2 animation
    _controller2 = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _animation2 = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
      parent: _controller2,
      curve: Curves.fastOutSlowIn,
    ))..addStatusListener(handler);
  }

  @override
  Widget build(BuildContext context) {
    
    final double width = MediaQuery.of(context).size.width;
    _controller.forward();
    _controller2.forward();

    return Column(
      children: <Widget>[
        AnimatedBuilder(
          animation: _controller,
          builder: (BuildContext context, Widget child) {
            return Center(
              child: Transform(
                transform: Matrix4.translationValues(_animation.value * width, 0.0, 0.0),
                child: new Center(
                  child: buildText()
                ),
              )
            );
          }
        ),
        AnimatedBuilder(
          animation: _controller2,
          builder: (BuildContext context, Widget child) {
            return Center(
              child: Transform(
                transform: Matrix4.translationValues(_animation2.value * width, 0.0, 0.0),
                child: new Center(
                  child: form
                  //child: buildForm()
                ),
              )
            );
          }
        ),
      ],
    );
  }

  buildText() {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 15.0, ),
          child: Text(
            "${form.state.nomeAtualWidget}",
            style: TextStyle(
              color: primaryColor,
              fontSize: 30,
              fontFamily: "Lobster",
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 35.0, 20.0, 20.0,),
          child: Text(
            "${form.state.nomeAtualWidget}",
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontFamily: "Lobster",
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }

  buildForm(){
    return Container(
      margin: EdgeInsets.only(left: 15.0, right: 15.0),
      padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            //focusNode: myFocusNodeEmailLogin,
            //controller: loginEmailController,
            keyboardType: TextInputType.emailAddress,
            textAlign: TextAlign.center,
            maxLength: 15,
            style: TextStyle(
              fontFamily: "WorkSansSemiBold",
              fontSize: 16.0,
              color: Colors.black),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Digite :)",
              hintStyle: TextStyle(
                fontSize: 17.0
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> handler(status) async {
    //antes de continuar a anima;áo espera 2 segundos
    //substituir por um comando de botao
    var boolean = false;
    if (status == AnimationStatus.completed  && boolean) {
      Future.delayed(const Duration(seconds: 2), () {
        _animation.removeStatusListener(handler);
        _controller.reset();

        _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Curves.fastOutSlowIn,))
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              //Navigator.pop(context); //VAI FECHAR A TELA
            }
          });
        _controller.forward();
      });
    }
  }

  moverOutForm({@required bool forBack})  {
    setState(() {
      //titulo animation out
      _animation.removeStatusListener(handler);
      _controller.reset();
      _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Curves.fastOutSlowIn,
      ));
      //body animation out
      _animation2.removeStatusListener(handler);
      _controller2.reset();
      _animation2 = Tween(begin: 0.0, end: -1.0).animate(CurvedAnimation(
        parent: _controller2,
        curve: Curves.fastOutSlowIn,
      ));
      Future.delayed(const Duration(milliseconds: 501), () {
        setState(() {
          form.state.proximoWidget(forBack, context); //trocando widget
          //new titulo animation in
          _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
          _animation = Tween(begin: -1.0, end: 0.0).animate(CurvedAnimation(
            parent: _controller,
            curve: Curves.fastOutSlowIn,
          ))..addStatusListener(handler);
          //new body animation in
          _controller2 = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
          _animation2 = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
            parent: _controller2,
            curve: Curves.fastOutSlowIn,
          ))..addStatusListener(handler);
          });
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _controller2.dispose();
    super.dispose();
  }
}
//// ttrocando posição
class FormNewDataWidget extends StatefulWidget {

  _FormNewDataWidgetState state = _FormNewDataWidgetState();

  @override
  _FormNewDataWidgetState createState() => state;
}

class _FormNewDataWidgetState extends State<FormNewDataWidget> {

  String nomeAtualWidget;
  var atualWidget;
  DataModel data;
  DataBaseHelper db;
  List<String> capitulos;

  @override
  initState() {
    super.initState();
    data = DataModel();
    //capitulos = await Util.getCapitulosJson();
    db = new DataBaseHelper();
    nomeAtualWidget = "";
    _buildTipoEventoWidget();
  }

  @override
  Widget build(BuildContext context) {
    return atualWidget;
  }

  //int atualWidgetID = 1;
  proximoWidget(bool forBack, BuildContext context){
    if(forBack && atualWidgetID >= 2)
      atualWidgetID -= 1;
    else if(!forBack && atualWidgetID < 5)
      atualWidgetID += 1;

    switch (atualWidgetID) {
      case 1:
        _buildTipoEventoWidget();
        break;
      case 2:
        _buildCapitulosWidget();
        break;
      case 3:
        _buildLocalWidget();
        break;
      case 4:
        _buildComentarioWidget();
        break;
      case 5:
        _buildSaveProgressWidget();
        db.saveData(data, context);
        break;
      default:
        break;
    }
  }

  TextEditingController tipoEventoController = TextEditingController();
  _buildTipoEventoWidget(){
    setState(() {
      nomeAtualWidget = "Qual o tipo de Evento?";
      atualWidget = Container(
        margin: EdgeInsets.only(left: 15.0, right: 15.0),
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
                controller: tipoEventoController,
                textAlign: TextAlign.center,
                maxLength: 15,
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Reunião, Ensaio, Culto? :)",
                  hintStyle: TextStyle(
                    fontSize: 16.0
                  ),
                ),
                onChanged: (val){
                  if(val.isNotEmpty){
                    validateBuildTipoEventoWidget = true;
                    data.tipoEvento = val;
                  }
                },
              ),
          ),
        ),
      );
    });
  }

  bool outrosCapitulos = false;
  double heightContainer = 0;
  bool visibilityContainer = false;
  TextEditingController livroController = TextEditingController();
  TextEditingController capituloFinalController = TextEditingController();
  _buildCapitulosWidget(){
    if(data.numeroVersiculoFinal == null || data.numeroVersiculoInicial == null){
      data.numeroVersiculoFinal = 1;
      data.numeroVersiculoInicial = 1;
      data.numeroCapituloInicial = 1;
    }
    GlobalKey<AutoCompleteTextFieldState<String>> key1 = new GlobalKey();
    GlobalKey<AutoCompleteTextFieldState<String>> key2 = new GlobalKey();
    
    setState(() {
      //nomeAtualWidget = "Capitulos";
      nomeAtualWidget = "Palavra";
      atualWidget = Container(
        margin: EdgeInsets.only(left: 15.0, right: 15.0),
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                SimpleAutoCompleteTextField(
                  key: key1,
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Livro",
                    hintStyle: TextStyle(
                      fontSize: 16.0
                    ),
                  ),
                  controller: livroController,
                  suggestions: capitulos,
                  textChanged: (text) => data.nomeCapitulo = text,
                  clearOnSubmit: false,
                  textSubmitted: (text) => setState(() {
                    if (text != "") {
                      data.nomeCapitulo = text;
                      livroController.text = text;
                      validateCapitulosWidget = true;
                      _buildCapitulosWidget();
                    }
                  }),
                ),
                Divider(),
                Column(
                  children: <Widget>[
                    Text("Capitulo"),
                    IconButton(
                      icon: Icon(Icons.arrow_drop_up),
                      onPressed: (){
                        setState(() {
                          data.numeroCapituloInicial++;
                          _buildCapitulosWidget(); //para atualizar o widget
                        });
                      }
                    ),
                    Text("${data.numeroCapituloInicial}"),
                    IconButton(
                      icon: Icon(Icons.arrow_drop_down),
                      onPressed: (){
                        setState(() {
                          data.numeroCapituloInicial--;
                          _buildCapitulosWidget(); //para atualizar o widget
                        });
                      }
                    ),
                  ],
                ),
                Divider(),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text("Versiculo Inicial"),
                          IconButton(
                            icon: Icon(Icons.arrow_drop_up),
                            onPressed: (){
                              setState(() {
                                data.numeroVersiculoInicial++;
                                if(data.numeroVersiculoFinal < data.numeroVersiculoInicial){
                                  data.numeroVersiculoFinal++;
                                }
                                _buildCapitulosWidget(); //para atualizar o widget
                              });
                            }
                          ),
                          Text("${data.numeroVersiculoInicial}"),
                          IconButton(
                            icon: Icon(Icons.arrow_drop_down),
                            onPressed: (){
                              setState(() {
                                if(data.numeroVersiculoInicial>1)
                                  data.numeroVersiculoInicial--;
                                _buildCapitulosWidget(); //para atualizar o widget
                              });
                            }
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text("Versiculo Final"),
                          IconButton(
                            icon: Icon(Icons.arrow_drop_up),
                            onPressed: (){
                              setState(() {
                                data.numeroVersiculoFinal++;
                                _buildCapitulosWidget(); //para atualizar o widget
                              });
                            }
                          ),
                          Text("${data.numeroVersiculoFinal}"),
                          IconButton(
                            icon: Icon(Icons.arrow_drop_down),
                            onPressed: (){
                              setState(() {
                                if(data.numeroVersiculoFinal > 1 && data.numeroVersiculoFinal > data.numeroVersiculoInicial){
                                  data.numeroVersiculoFinal--;
                                }
                                _buildCapitulosWidget(); //para atualizar o widget
                              });
                            }
                          ),
                        ],
                      )
                    ],
                  )
                ),
                Visibility( //TODO: SO ESCONDENDO POR ENQUANTO
                  visible: false,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Termina em outro Capitulo?"),
                      Checkbox(
                        value: outrosCapitulos,
                        onChanged: (bool value) {
                          setState(() {
                            outrosCapitulos = value;
                            if(heightContainer == 0)
                              heightContainer = 60.0;
                            else{
                              heightContainer = 0.0;
                              visibilityContainer = false;
                            }
                            _buildCapitulosWidget(); //para atualizar o widget
                          });
                        },
                      ),
                    ],
                  ),
                ),
                Divider(),
                AnimatedContainer(
                  duration: new Duration(milliseconds: 300),
                  height: heightContainer,
                  onEnd: (){
                    setState(() {
                      if(heightContainer == 0.0)
                        visibilityContainer = false;
                      else
                        visibilityContainer = true;

                      _buildCapitulosWidget(); //para atualizar o widget
                    });
                  },
                  child: Visibility(
                    visible: visibilityContainer,
                    child: SimpleAutoCompleteTextField(
                      key: key2,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black
                      ),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Capitulo Final",
                        hintStyle: TextStyle(
                          fontSize: 16.0
                        ),
                      ),
                      controller: capituloFinalController,
                      suggestions: capitulos,
                      //textChanged: (text) => currentTextCapFinal = text,
                      clearOnSubmit: false,
                      textSubmitted: (text) => setState(() {
                        if (text != "") {
                          print(text);
                          capituloFinalController.text = text;
                          _buildCapitulosWidget();
                        }
                      }),
                    ),
                  )
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  TextEditingController cidadeController = TextEditingController();
  TextEditingController bairroOrIgrejaController = TextEditingController();
  _buildLocalWidget(){
    setState(() {
      nomeAtualWidget = "Local";
      atualWidget = Container(
        margin: EdgeInsets.only(left: 15.0, right: 15.0),
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                TextField(
                  textAlign: TextAlign.center,
                  controller: cidadeController,
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Cidade",
                    hintStyle: TextStyle(
                      fontSize: 16.0
                    ),
                  ),
                  onChanged: (text) => setState(() {
                    if (text != "") {
                      data.cidade= text;
                      validateLocalWidget1 = true;
                      _buildLocalWidget();
                    }
                  }),
                ),
                Divider(),
                TextField(
                  textAlign: TextAlign.center,
                  controller: bairroOrIgrejaController,
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Bairro/Nome Igreja",
                    hintStyle: TextStyle(
                      fontSize: 16.0
                    ),
                  ),
                  onChanged: (val){
                    if(val.isNotEmpty){
                      data.localCidadeOuIgreja = val;
                      validateLocalWidget2 = true;
                    }
                  },
                ),
                Divider(),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: FlatButton(
                    onPressed: () async {
                      DateTime dia = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2000), lastDate: DateTime.now());
                      if(dia != null)
                        setState(() {
                          data.data = dia.day.toString() + "/" + dia.month.toString() + "/" + dia.year.toString();
                          _buildLocalWidget();
                          validateLocalWidget3 = true;
                        });
                    },
                    child: Text(
                      data.data == null? "Data:" : "Data: ${data.data}",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 16.0,
                      ),
                    )
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  TextEditingController comentarioController = TextEditingController();
  _buildComentarioWidget(){
    setState(() {
      nomeAtualWidget = "Sobre a Palavra";
      atualWidget = Container(
        margin: EdgeInsets.only(left: 15.0, right: 15.0),
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              //focusNode: myFocusNodeEmailLogin,
              controller: comentarioController,
              maxLines: 5,
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Comentario",
                hintStyle: TextStyle(
                  fontSize: 16.0
                ),
              ),
              onChanged: (val){
                if(val.isNotEmpty){
                  data.comentario = val;
                  validateComentarioWidget = true;
                }
              },
            ),
          ),
        ),
      );
    });
  }

  _buildSaveProgressWidget(){
    setState(() {
      nomeAtualWidget = "Salvando...";
      atualWidget = Container(
        margin: EdgeInsets.only(left: 15.0, right: 15.0),
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircularProgressIndicator()
        ),
      );
    });
  }
}