import 'package:flutter/material.dart';

  var primaryColor = const Color(0xFFda5e5a);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFe89b98);

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  
  PageController controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          int page = controller.page.round() + 1;
          controller.animateToPage(page, curve: Curves.ease, duration: Duration(milliseconds: 300));
        },
        heroTag: "fab002",
        icon: Icon(Icons.add_circle_outline),
        label: Text("Proxima")
      ),
      body: PageView(
        controller: controller,
        onPageChanged: (num){
          
        },
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Container(
            color: terciaryColor,
          ),
          Container(
            color: Colors.cyan,
          ),
          Container(
            color: Colors.deepPurple,
          ),
        ],
      ),
    );
  }
}