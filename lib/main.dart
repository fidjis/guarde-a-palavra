import 'package:flutter/material.dart';

import 'screens/home_screen.dart';
import 'screens/login_screen.dart';
import 'screens/new_data_screen.dart';
import 'screens/splash_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Guarde a Palavra',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/splash_page',
      routes: <String, WidgetBuilder> {
        '/home_screen': (context) => HomeScreen(), 
        '/login_screen': (context) => Login(), 
        '/new_data': (context) => NewData(),
        '/splash_page': (context) => SplashPage(),
      },
    );
  }
}