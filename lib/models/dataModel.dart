class DataModel{
  
  String _id;
  String _tipoEvento;
  String _nomeCapitulo;
  int _numeroCapituloInicial;
  int _numeroVersiculoInicial;
  int _numeroVersiculoFinal;
  String _cidade;
  String _localCidadeOuIgreja;
  String _data;
  String _comentario;

  String get id => _id;

  set id(String id) {
    _id = id;
  }
  
  String get tipoEvento => _tipoEvento;

  set tipoEvento(String tipoEvento) {
    _tipoEvento = tipoEvento;
  }

  String get comentario => _comentario;

  set comentario(String comentario) {
    _comentario = comentario;
  }

  String get nomeCapitulo => _nomeCapitulo;

  set nomeCapitulo(String nomeCapitulo) {
    _nomeCapitulo = nomeCapitulo;
  }

  int get numeroCapituloInicial => _numeroCapituloInicial;

  set numeroCapituloInicial(int numeroCapituloInicial) {
    _numeroCapituloInicial = numeroCapituloInicial;
  }

  int get numeroVersiculoInicial => _numeroVersiculoInicial;

  set numeroVersiculoInicial(int numeroVersiculoInicial) {
    _numeroVersiculoInicial = numeroVersiculoInicial;
  }

  int get numeroVersiculoFinal => _numeroVersiculoFinal;

  set numeroVersiculoFinal(int numeroVersiculoFinal) {
    _numeroVersiculoFinal = numeroVersiculoFinal;
  }

  String get cidade => _cidade;

  set cidade(String cidade) {
    _cidade = cidade;
  }

  String get localCidadeOuIgreja => _localCidadeOuIgreja;

  set localCidadeOuIgreja(String localCidadeOuIgreja) {
    _localCidadeOuIgreja = localCidadeOuIgreja;
  }

  String get data => _data;

  set data(String data) {
    _data = data;
  }

  
}