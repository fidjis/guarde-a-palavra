
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

var primaryColor = const Color(0xFFda5e5a);
var segundaryColor = Colors.white;
var terciaryColor = const Color(0xFFe89b98);

class MyItemExpanded {
  MyItemExpanded({this.TAG, this.isExpanded: false, this.header, this.body });

  final String TAG;
  bool isExpanded;
  final Widget header;
  final Widget body;
}

class MyHeader extends StatefulWidget {

  MyHeader({this.headerName});

  final String headerName;
  final _MyHeaderState state = _MyHeaderState();

  @override
  _MyHeaderState createState() => state;
}

class _MyHeaderState extends State<MyHeader> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: BoxDecoration(
                  border: Border(
                      right: BorderSide(width: 1.0, color: primaryColor))),
              child: Icon(Icons.music_note, color: primaryColor),
            ),
            title: Text(
              widget.headerName,
              style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              "Apocalipse 6 X Apocalipse 6",
              style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

class MyItemBody extends StatefulWidget {

  MyItemBody({this.name});

  final String name;
  final _MyItemBodyState state = _MyItemBodyState();

  @override
  _MyItemBodyState createState() => state;
}

class _MyItemBodyState extends State<MyItemBody> {

  Future<List<String>> getVersiculos(int livro, int cap, int vInicial, int vFinal, ) async {

    DataSnapshot data = await FirebaseDatabase.instance
      .reference()
      .child("biblia")
      .child("ptbr")
      .child("aa")
      .child(livro.toString())
      //.child("2")
      .child("chapters")
      .child((cap - 1).toString())
      //.child("1")
      .once();
    
    List<String> listVersiculoss = List<String>();
    if(data.value != null)
      data.value.forEach((childSnapshot) {
        listVersiculoss.add(childSnapshot);
      });
      

    return listVersiculoss;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: terciaryColor,
      child: FutureBuilder(
        future: getVersiculos(0, 119, 1, 117, ),
        builder: (context, snapshot){
          if (!snapshot.hasData) {
            return CircularProgressIndicator();
          }
          if (snapshot.hasData) {
              List<String> listVersiculos = snapshot.data;
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: listVersiculos.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  print(listVersiculos[index]);
                  return ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
                    leading: Text(
                      "v${index+1}",
                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    title: Text(
                      "${listVersiculos[index]}",
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                  );
                }
              );
          }else{
            return Text("Não encontrado", style: TextStyle(color: Colors.white, fontSize: 14));
          }
        }
      ),
    );
  }
}