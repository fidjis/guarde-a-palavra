import 'package:flutter/material.dart';
import 'package:guarde_a_palavra/models/dataModel.dart';
import 'package:guarde_a_palavra/screens/view_data_screen.dart';

class DataWidget extends StatefulWidget {

  DataWidget({
    @required this.tipoEvento, 
    @required this.livro, 
    @required this.data, 
    @required this.numCapitulo, 
    @required this.numVersiculoInicial, 
    @required this.numVersiculoFinal, 
    @required this.cidade, 
    @required this.bairro
  });

  final String tipoEvento;
  final String livro;
  final String data;
  final String numCapitulo;
  final String numVersiculoInicial;
  final String numVersiculoFinal;
  final String cidade;
  final String bairro;

  @override
  _DataWidgetState createState() => _DataWidgetState();
}

class _DataWidgetState extends State<DataWidget> {
  var primaryColor = const Color(0xFFda5e5a);
  var segundaryColor = Colors.white;
  var terciaryColor = const Color(0xFFe89b98);
  DataModel data = new DataModel();

  @override
  void initState() {
    super.initState();
    data.tipoEvento = widget.tipoEvento;
    data.nomeCapitulo = widget.livro;
    data.data = widget.data;
    data.numeroCapituloInicial = int.parse(widget.numCapitulo);
    data.numeroVersiculoInicial = int.parse(widget.numVersiculoInicial);
    data.numeroVersiculoFinal = int.parse(widget.numVersiculoFinal);
    data.cidade = widget.cidade;
    data.localCidadeOuIgreja = widget.bairro;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        buildIndividualCard(),
        //buildIndividualCard(),
        //buildGrupCard([new DataModel(), new DataModel()]),
        //buildIndividualCard(),
        //buildIndividualCard(),
        //buildGrupCard([new DataModel(), new DataModel()]),
        //buildIndividualCard(),
      ],
    );
  }


  buildIndividualCard() {
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ViewDataScreen(
              data
            )));
      },
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 60,
          ),
          Expanded(
            child: Card(
              margin: EdgeInsets.only(right: 10.0, bottom: 5.0),
              color: Colors.white70,
              child: Container(
                padding: EdgeInsets.all(10.0),
                height: 85.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "${widget.livro}",
                          style: TextStyle(
                            fontSize: 16,
                            //color: primaryColor
                          ),
                        ),
                        Text(
                          "${widget.data}",
                          style: TextStyle(
                            fontSize: 16,
                            //color: primaryColor
                          ),
                        )
                      ],
                    ),
                    Divider(),
                    Text(
                      "Cap ${widget.numCapitulo}. v${widget.numVersiculoInicial} ao v${widget.numVersiculoFinal}",
                      style: TextStyle(
                        fontSize: 10,
                        //color: Colors.white
                      ),
                    ),
                    Text(
                      "${widget.tipoEvento} - ${widget.cidade} - ${widget.bairro}",
                      style: TextStyle(
                        fontSize: 10,
                        //color: Colors.white
                      ),
                    ),
                  ],
                )
              ),
            ),
          ),
        ],
      ),
    );
  }
  
  buildGrupCard(List<DataModel> dataModels) {
    List<Widget> list = new List<Widget>();
    dataModels.forEach((data){
      list.add(Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 60,
          ),
          Expanded(
            child: Card(
              margin: EdgeInsets.only(right: 10.0, bottom: 0.5),
              color: Colors.white70,
              child: Container(
                padding: EdgeInsets.all(10.0),
                height: 85.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          " ",
                          style: TextStyle(
                            fontSize: 16,
                            //color: primaryColor
                          ),
                        ),
                        Text(
                          " ",
                          style: TextStyle(
                            fontSize: 16,
                            //color: primaryColor
                          ),
                        )
                      ],
                    ),
                    Divider(),
                    Text(
                      " ",
                      style: TextStyle(
                        fontSize: 10,
                        //color: Colors.white
                      ),
                    ),
                    Text(
                      " ",
                      style: TextStyle(
                        fontSize: 10,
                        //color: Colors.white
                      ),
                    ),
                  ],
                )
              ),
            ),
          ),
        ],
      ));
    });
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned(
          left: 0.0,
          child: Container(
            margin: EdgeInsets.only(right: 5.0, left: 5.0),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 5.0,
            height: 65.0 * dataModels.length,
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: list,
          ),
        ),
      ],
    );
  }
}